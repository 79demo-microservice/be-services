package com.example.orderservice.controller;

import java.util.UUID;

import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.basedomains.dto.Order;
import com.example.basedomains.dto.OrderEvent;
import com.example.orderservice.OrderProducer;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class OrderController {

  private final OrderProducer orderProducer;

  @PostMapping("/orders")
  public ResponseEntity<Object> placeOrder(@RequestBody Order order) {
    order.setOrderId(UUID.randomUUID().toString());

    var orderEvent = OrderEvent.builder()
        .status("PENDING")
        .message("Order Status is in Pending State")
        .order(order)
        .build();

    orderProducer.sendMessage(orderEvent);

    return ResponseEntity.status(HttpStatus.SC_CREATED).build();
  }

}
